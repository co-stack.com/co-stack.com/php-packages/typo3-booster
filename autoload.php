<?php

declare(strict_types=1);

/**
 * @var string $file
 */
$composerPath = explode('vendor/composer', $file)[0] . 'vendor/_generated';

if ($_SERVER['SCRIPT_NAME'] === '/index.php') {
    if (!file_exists($composerPath . '/preload_frontend.php')) {
        return;
    }
    require $composerPath . '/preload_frontend.php';
} elseif ($_SERVER['SCRIPT_NAME'] === '/typo3/index.php') {
    if (!file_exists($composerPath . '/preload_backend.php')) {
        return;
    }
    require $composerPath . '/preload_backend.php';
} elseif (PHP_SAPI === 'cli') {
    if (!file_exists($composerPath . '/preload_cli.php')) {
        return;
    }
    require $composerPath . '/preload_cli.php';
}
