<?php

use ClassPreloader\ClassLoader;
use TYPO3\CMS\Core\Console\CommandApplication;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Core\SystemEnvironmentBuilder;

$classLoader = require $GLOBALS['_composer_autoload_path'];

$fileList = ClassLoader::getIncludes(static function (ClassLoader $loader) use($classLoader): void {
    $loader->register();

    SystemEnvironmentBuilder::run(1, SystemEnvironmentBuilder::REQUESTTYPE_CLI);
    Bootstrap::init($classLoader, true)->get(CommandApplication::class);
});

$fileList->addExclusiveFilter('~InstalledVersions~');
$fileList->addExclusiveFilter('~LogLevel~');
$fileList->addExclusiveFilter('~LoggerInterface~');
$fileList->addExclusiveFilter('~Logger~');
$fileList->addExclusiveFilter('~EventDispatcher~');
$fileList->addExclusiveFilter('~ResetInterface~');

return $fileList;
