<?php

use ClassPreloader\ClassLoader;
use TYPO3\CMS\Backend\Http\Application;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Core\SystemEnvironmentBuilder;

$classLoader = require $GLOBALS['_composer_autoload_path'];

$fileList = ClassLoader::getIncludes(static function (ClassLoader $loader) use($classLoader): void {
    $loader->register();

    SystemEnvironmentBuilder::run(1, SystemEnvironmentBuilder::REQUESTTYPE_BE);
    Bootstrap::init($classLoader)->get(Application::class);
});

$fileList->addExclusiveFilter('~ArrayUtility~');
$fileList->addExclusiveFilter('~InstalledVersions~');
$fileList->addExclusiveFilter('~LogLevel~');
$fileList->addExclusiveFilter('~LoggerInterface~');
$fileList->addExclusiveFilter('~Logger~');
$fileList->addExclusiveFilter('~EventDispatcher~');
$fileList->addExclusiveFilter('~ResetInterface~');
$fileList->addExclusiveFilter('~typo3/cms-extensionmanager/Classes/Report/ExtensionStatus.php~');
$fileList->addExclusiveFilter('~symfony/dependency-injection/Argument/ServiceLocator.php~');

return $fileList;
