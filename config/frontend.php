<?php

use ClassPreloader\ClassLoader;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Core\SystemEnvironmentBuilder;
use TYPO3\CMS\Frontend\Http\Application;

$classLoader = require $GLOBALS['_composer_autoload_path'];

$fileList = ClassLoader::getIncludes(static function (ClassLoader $loader) use ($classLoader): void {
    $loader->register();

    SystemEnvironmentBuilder::run(0, SystemEnvironmentBuilder::REQUESTTYPE_FE);
    Bootstrap::init($classLoader)->get(Application::class);
});

$fileList->addExclusiveFilter('~typo3/cms-extensionmanager/Classes/Report/ExtensionStatus.php~');

return $fileList;
