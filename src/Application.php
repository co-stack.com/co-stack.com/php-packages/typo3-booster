<?php

declare(strict_types=1);

namespace CoStack\Typo3Booster;

use CoStack\Typo3Booster\Command\CompileCommand;

class Application extends \Symfony\Component\Console\Application
{
    public function __construct(string $name = 'UNKNOWN', string $version = 'UNKNOWN')
    {
        parent::__construct($name, $version);

        $this->add(new CompileCommand());
    }
}
