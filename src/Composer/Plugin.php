<?php

declare(strict_types=1);

namespace CoStack\Typo3Booster\Composer;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\ScriptEvents;
use Symfony\Component\Process\Process;

use function rtrim;

class Plugin implements PluginInterface, EventSubscriberInterface
{
    private Composer $composer;
    private IOInterface $io;

    public function activate(Composer $composer, IOInterface $io): void
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    public function deactivate(Composer $composer, IOInterface $io): void
    {
        // Nothing to do
    }

    public function uninstall(Composer $composer, IOInterface $io): void
    {
        // Nothing to do
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ScriptEvents::POST_AUTOLOAD_DUMP => 'postAutoloadDump',
        ];
    }

    public function postAutoloadDump(): void
    {
        $binDir = $this->composer->getConfig()->get('bin-dir');
        $vendorDir = $this->composer->getConfig()->get('vendor-dir');

        $typo3booster = [];
        $typo3booster[] = rtrim($binDir, '/') . '/typo3booster';
        $typo3booster[] = 'compile';
        $typo3booster[] = $binDir;
        $typo3booster[] = $vendorDir;

        $process = new Process($typo3booster);
        $process->setEnv($_ENV);
        $this->io->write('Compiling preloader files');
        $process->run(
            fn(string $channel, string $line) => $channel === Process::ERR
                ? $this->io->writeError($line, false)
                : $this->io->write($line, false),
        );
    }
}
