<?php

declare(strict_types=1);

namespace CoStack\Typo3Booster\Command;

use CoStack\ProcessManager\ProcessManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

use function basename;
use function CoStack\Lib\concat_paths;
use function dirname;
use function rtrim;
use function unlink;

class CompileCommand extends Command
{
    protected function configure()
    {
        $this->setName('compile')
             ->addArgument('bin-dir', InputArgument::REQUIRED)
             ->addArgument('vendor-dir', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $binDir = rtrim($input->getArgument('bin-dir'), '/');
        $vendorDir = rtrim($input->getArgument('vendor-dir'), '/');

        $errorOutput = $output instanceof ConsoleOutput ? $output->getErrorOutput() : $output;

        $packagePath = dirname(__DIR__, 2);
        $scripts = [
            concat_paths($packagePath, 'config/frontend.php'),
            concat_paths($packagePath, 'config/backend.php'),
            concat_paths($packagePath, 'config/cli.php'),
        ];

        /** @var Process[] $processes */
        $processes = [];
        foreach ($scripts as $script) {
            $outputFile = concat_paths($vendorDir, '_generated/preload_' . basename($script));
            if (file_exists($outputFile)) {
                unlink($outputFile);
            }
            $command = [];
            $command[] = concat_paths($binDir, 'classpreloader');
            $command[] = 'compile';
            $command[] = '--config=' . $script;
            $command[] = '--output=' . $outputFile;
            $processes[] = new Process($command, null, ['PHP_IDE_CONFIG' => 'serverName=local.typo3-booster.com']);
        }
        $processManager = new ProcessManager();
        $processManager->runParallel($processes, 3);

        foreach ($processes as $process) {
            if (!$process->isSuccessful()) {
                $errorOutput->write('Process "' . $process->getCommandLine() . '" failed:');
                $errorOutput->write($process->getOutput());
                $errorOutput->write($process->getErrorOutput());
            }
        }

        $output->writeln('Compiled all preload files');
        return Command::SUCCESS;
    }
}
